import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class TheFrogDashboard extends JFrame {
    private static JFileChooser fc;
    private final JButton selectFileButton;
    private JButton clearButton;
    private JTabbedPane tabbedPane1;
    private JPanel scanPanel;

    public TheFrogDashboard() {
        JFrame frame = new JFrame("The Frog Dashboard");
        frame.setSize(500, 500);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);

        selectFileButton = new JButton("Select Your File");
        selectFileButton.setBounds(100, 400, 300, 50);
        selectFileButton.setFocusPainted(false);
        selectFileButton.setVisible(true);
        selectFileButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                try {
                    openFile(e);
                } catch (FileNotFoundException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                }
            }
        });

        JPanel theFrogDashboard = new JPanel();
        theFrogDashboard.setBounds(200, 400, 100, 50);

        frame.add(selectFileButton);
        frame.add(theFrogDashboard);
    }

    private void openFile(MouseEvent e) throws FileNotFoundException {
        Scanner fileScanner;
        if (e.getSource() == selectFileButton) {
            JFileChooser fileChooser = new JFileChooser();
            int response = fileChooser.showOpenDialog(null);

            if (response == JFileChooser.APPROVE_OPTION) {
                File file = new File(fileChooser.getSelectedFile().getAbsolutePath());
                System.out.println(file);

                fileScanner = new Scanner(file);
                if (file.isFile()) {
                    while (fileScanner.hasNextLine()) {
                        String line = fileScanner.nextLine();
                        System.out.println(line);
                    }
                }
            }
        }
    }
}